%# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
%# Use of this source code is governed by a BSD-style license that can be
%# found in the LICENSE file.
%
%from src import settings
%_root = settings.settings.relative_root
%
%# --------------------------------------------------------------------------
%# LEGEND
%_hide_missing = [v.lower() for k, v in query_string(list_qps=True)
%                        if k.lower() == 'hide_missing']
%if _hide_missing and _hide_missing[0] == 'false':
%  _hide_missing = False
%end
%_show_faft_view = [v.lower() for k, v in query_string(list_qps=True)
%                        if k.lower() == 'show_faft_view']
%if _show_faft_view and _show_faft_view[0] == 'false':
%  _show_faft_view = False
%end
<div>
  <table>
    <tbody>
    <tr>
      <td class="legend headeritem">Legend&nbsp;&nbsp;</td>
      %if not _hide_missing:
        <td class="legend missing_suites"
            title="Expected suite results were missing.">Missing Suite</td>
      %end
      <td class="legend success"
          title="Tests that passed (GOOD).">Passed</td>
      <td class="legend failure"
          title="There is a new failure (FAIL). Take a look!">Failed</td>
      <td class="legend warning_summary"
          title="Test problem other than FAIL occured: ABORT, ERROR, TEST_NA, WARN.">
          Other</td>
      %if not _hide_missing:
        <td class="legend missing_tests"
            title="Expected test results were missing.">Missing Test</td>
      %end
      <td class="legend"
          title="Test not run.">No&nbsp;data</td>
      %if not _hide_missing:
        <td class="infotext">
        <a class="tooltip"
           onmouseover="fixTooltip(event, this)"
           onmouseout="clearTooltip(this)"
           href="{{ _root }}/matrix/{{ tpl_vars['filter_tag'] }}{{! query_string({'hide_missing': True}) }}">
          hide missing
          <span>
            Hide missing suites and tests.
          </span>
        </a>
        </td>
      %end
      %if not _show_faft_view:
        <td class="infotext">
        <a class="tooltip"
           onmouseover="fixTooltip(event, this)"
           onmouseout="clearTooltip(this)"
           href="{{ _root }}/matrix/{{ tpl_vars['filter_tag'] }}{{! query_string({'show_faft_view': True}) }}">
          show faft view
          <span>
            Show tests with firmware updated.
          </span>
        </a>
        </td>
      %end
    </tr>
    </tbody>
  </table>
</div>
<br>
