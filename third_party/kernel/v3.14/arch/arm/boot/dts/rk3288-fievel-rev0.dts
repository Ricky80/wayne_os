/*
 * Google Veyron Fievel Rev 0+ board device tree source
 *
 * Copyright 2016 Google, Inc
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

/dts-v1/;
#include "rk3288-veyron.dtsi"
#include "rk3288-veyron-analog-audio.dtsi"
#include "rk3288-veyron-ethernet.dtsi"
#include "rk3288-veyron-lpddr.dtsi"

/ {
	model = "Google Fievel";
	compatible = "google,veyron-fievel-rev8", "google,veyron-fievel-rev7",
		     "google,veyron-fievel-rev6", "google,veyron-fievel-rev5",
		     "google,veyron-fievel-rev4", "google,veyron-fievel-rev3",
		     "google,veyron-fievel-rev2", "google,veyron-fievel-rev1",
		     "google,veyron-fievel-rev0", "google,veyron-fievel",
		     "google,veyron", "rockchip,rk3288";

	/* vcc33_pmuio and vcc33_io is sourced directly from vcc33_sys,
	 * enabled by vcc_18 */
	vcc33_io: vcc33-io {
		compatible = "regulator-fixed";
		regulator-always-on;
		regulator-boot-on;
		regulator-name = "vcc33_io";
	};

	vcc5_host1: vcc5-host1-regulator {
		compatible = "regulator-fixed";
		enable-active-high;
		gpio = <&gpio5 18 GPIO_ACTIVE_HIGH>;
		pinctrl-names = "default";
		pinctrl-0 = <&hub_usb1_pwr_en>;
		regulator-name = "vcc5_host1";
		regulator-always-on;
		regulator-boot-on;
	};

	vcc5_host2: vcc5-host2-regulator {
		compatible = "regulator-fixed";
		enable-active-high;
		gpio = <&gpio5 14 GPIO_ACTIVE_HIGH>;
		pinctrl-names = "default";
		pinctrl-0 = <&hub_usb2_pwr_en>;
		regulator-name = "vcc5_host2";
		regulator-always-on;
		regulator-boot-on;
	};

	vcc5v_otg: vcc5v-otg-regulator {
		compatible = "regulator-fixed";
		enable-active-high;
		gpio = <&gpio0 12 GPIO_ACTIVE_HIGH>;
		pinctrl-names = "default";
		pinctrl-0 = <&usb_otg_pwr_en>;
		regulator-name = "vcc5_otg";
		regulator-always-on;
		regulator-boot-on;
	};

	vccsys: vccsys {
		compatible = "regulator-fixed";
		regulator-name = "vccsys";
		regulator-boot-on;
		regulator-always-on;
	};
};

&dmc {
	status = "disabled";
};

&gmac {
	phy-supply = <&vcc33_lan>;
	wakeup-source;
	mdio0 {
		#address-cells = <1>;
		#size-cells = <0>;
		compatible = "snps,dwmac-mdio";
		phy1: ethernet-phy@1 {
			reg = <1>;
			enable-phy-ssc;
		};
	};
};

&gpio_keys {
	pinctrl-0 = <&pwr_key_l>;

	/* let btmrvl handle bt wake */
	/delete-node/ bt-wake;
};

&rk808 {
	pinctrl-names = "default";
	pinctrl-0 = <&pmic_int_l &dvs_1 &dvs_2>;
	dvs-gpios = <&gpio7 12 GPIO_ACTIVE_HIGH>,
		    <&gpio7 15 GPIO_ACTIVE_HIGH>;

	vcc6-supply = <&vcc33_sys>;
	vcc10-supply = <&vcc33_sys>;
	vcc11-supply = <&vcc_5v>;
	vcc12-supply = <&vcc33_sys>;

	regulators {
		/delete-node/ LDO_REG1;

		/* According to the schematic, vcc18_lcdt is for
		 * HDMI_AVDD_1V8
		 */
		vcc18_lcdt: LDO_REG2 {
			regulator-always-on;
			regulator-boot-on;
			regulator-min-microvolt = <1800000>;
			regulator-max-microvolt = <1800000>;
			regulator-name = "vdd18_lcdt";
			regulator-suspend-mem-disabled;
		};

		/* This is not a pwren anymore, but the real power supply,
		 * vdd10_lcd for HDMI_AVDD_1V0
		 */
		vdd10_lcd: LDO_REG7 {
			regulator-always-on;
			regulator-boot-on;
			regulator-min-microvolt = <1000000>;
			regulator-max-microvolt = <1000000>;
			regulator-name = "vdd10_lcd";
			regulator-suspend-mem-disabled;
		};

		/* for usb camera */
		vcc33_ccd: LDO_REG8 {
			regulator-always-on;
			regulator-boot-on;
			regulator-min-microvolt = <3300000>;
			regulator-max-microvolt = <3300000>;
			regulator-name = "vcc33_ccd";
			regulator-suspend-mem-disabled;
		};

		vcc33_lan: SWITCH_REG2 {
			regulator-name = "vcc33_lan";
		};
	};
};

&sdio0 {
	#address-cells = <1>;
	#size-cells = <0>;

	btmrvl: btmrvl@2 {
		compatible = "marvell,sd8897-bt";
		reg = <2>;
		interrupt-parent = <&gpio4>;
		interrupts = <31 IRQ_TYPE_LEVEL_LOW>;
		marvell,wakeup-pin = /bits/ 16 <13>;
		pinctrl-names = "default";
		pinctrl-0 = <&bt_host_wake_l>;
	};
};

&vcc50_hdmi {
	enable-active-high;
	gpio = <&gpio5 19 GPIO_ACTIVE_HIGH>;
	pinctrl-names = "default";
	pinctrl-0 = <&vcc50_hdmi_en>;
};

&vcc_5v {
	enable-active-high;
	gpio = <&gpio7 21 GPIO_ACTIVE_HIGH>;
	pinctrl-names = "default";
	pinctrl-0 = <&drv_5v>;
};

&pinctrl {
	pinctrl-names = "default", "sleep";
	pinctrl-0 = <
		/* Common for sleep and wake, but no owners */
		&ddr0_retention
		&ddrio_pwroff
		&global_pwroff

		/* For usb bc1.2 */
		&usb_otg_ilim_sel
		&usb_usb_ilim_sel

		/* Wake only */
		&bt_dev_wake_awake
		&pwr_led1_on
	>;
	pinctrl-1 = <
		/* Common for sleep and wake, but no owners */
		&ddr0_retention
		&ddrio_pwroff
		&global_pwroff

		/* Sleep only */
		&bt_dev_wake_sleep
		&pwr_led1_blink
	>;

	buck-5v {
		drv_5v: drv-5v {
			rockchip,pins = <7 21 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	hdmi {
		vcc50_hdmi_en: vcc50-hdmi-en {
			rockchip,pins = <5 19 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};

	leds {
		pwr_led1_on: pwr-led1-on {
			rockchip,pins = <7 3 RK_FUNC_GPIO &pcfg_output_low>;
		};

		pwr_led1_blink: pwr-led1-blink {
			rockchip,pins = <7 3 RK_FUNC_GPIO &pcfg_output_high>;
		};
	};

	pmic {
		dvs_1: dvs-1 {
			rockchip,pins = <7 12 RK_FUNC_GPIO &pcfg_pull_down>;
		};

		dvs_2: dvs-2 {
			rockchip,pins = <7 15 RK_FUNC_GPIO &pcfg_pull_down>;
		};
	};

	usb-bc12 {
		usb_otg_ilim_sel: usb-otg-ilim-sel {
			rockchip,pins = <6 17 RK_FUNC_GPIO &pcfg_output_low>;
		};

		usb_usb_ilim_sel: usb-usb-ilim-sel {
			rockchip,pins = <5 15 RK_FUNC_GPIO &pcfg_output_low>;
		};
	};

	usb-host {
		hub_usb1_pwr_en: hub_usb1_pwr_en {
			rockchip,pins = <5 18 RK_FUNC_GPIO &pcfg_pull_none>;
		};

		hub_usb2_pwr_en: hub_usb2_pwr_en {
			rockchip,pins = <5 14 RK_FUNC_GPIO &pcfg_pull_none>;
		};

		usb_otg_pwr_en: usb_otg_pwr_en {
			rockchip,pins = <0 12 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};
};
