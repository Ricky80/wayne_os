/*
 *	Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
 *	Use of this source code is governed by a BSD-style license that can be
 *	found in the LICENSE file.
 */

/*
 * NAME
 *  cros_unittest_sample
 *
 * DESCRIPTION
 *  Testcase to demonstrate simple kernel unittests for ChromeOS.
 *  While cros_sample_getcwd02 is a sample ChromeOS test, it uses the LTP
 *  test template.  This test shows a method of writing a simpler test
 *  that does not use the LTP template pattern. Instead, this test is
 *  simply expected to run and return 0 (success) or nonzero (failure).
 *
 *  A ChromeOS .sh wrapper will be automatically generated with any
 *  needed LTP library calls to allow the test to run under the LTP harness.
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

char *pwd = "/bin/pwd";

char pwd_buf[BUFSIZ];  //holds results of pwd pipe
char cwd[BUFSIZ];			 //used as our valid buffer
char *buffer = NULL;	//catches the return value from getcwd when passing NULL
char *cwd_ptr = NULL;  //catches the return value from getcwd when passing cwd[]

int main()
{
	FILE *fin;
	char *cp;

	/*
	 * valid cwd[]: -> Should work fine
	 */
	if ((fin = popen(pwd, "r")) == NULL) {
		fprintf(stderr, "Cannot run %s\n", pwd);
		exit(1);
	}
	while (fgets(pwd_buf, sizeof(pwd_buf), fin) != NULL) {
		if ((cp = strchr(pwd_buf, '\n')) == NULL) {
			pclose(fin);
			fprintf(stderr, "No newline\n");
			exit(1);
		}
		*cp = 0;
	}
	pclose(fin);

	if ((cwd_ptr = getcwd(cwd, sizeof(cwd))) == NULL) {
		fprintf(stderr, "getcwd() returned NULL\n");
		exit(1);
	}
	if (strcmp(pwd_buf, cwd) != 0) {
		fprintf(stderr, "No match (%s, %s)\n", pwd_buf, cwd);
		exit(1);
	}
	exit(0);
}
